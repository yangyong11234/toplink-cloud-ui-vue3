const useModelStore = defineStore(
    'model',
    {
        state: () => ({
            buttonForm: [],
            elementId:''
        }),
        actions: {
            SET_BUTTONFORM(buttonForm) {
                this.buttonForm = buttonForm
            },
            SET_ELEMENTID(elementId) {
                this.elementId = elementId
            }
        }
    }
)
// const state = {
//     buttonForm: [],
//     elementId:''
// }
// const mutations = {
//     SET_BUTTONFORM:(state, buttonForm)=> {
//         state.buttonForm = buttonForm
//     },
//     SET_ELEMENTID:(state,elementId)=>{
//         state.elementId = elementId
//     }
// }

export default useModelStore

