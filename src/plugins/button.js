import auth from '@/plugins/auth'

// 公共页面按钮权限控制
export default {
    power(json) {
        let name = json.businessName;
        let arr = ['add', 'edit', 'remove', 'import', 'export', 'detail'];
        let all = ""
        arr.forEach(item => {
            all = `heating:${name}:${item}`
            getObjects(json.indexJson,'id','button' + item.charAt(0).toUpperCase() + item.substring(1),!auth.hasPermi(all))
            // json.index.widgetList[3].cols[0].widgetList[0].cols.forEach(e => {
            //     // console.log(e,'e',e.widgetList[0].id.replace('button', '').toLowerCase(),item);
            //     // console.log(e.widgetList[0].id.replace('button', '').toLowerCase(),'e.widgetList[0].id.replace');
            //     if (e.widgetList[0].id.replace('button', '').toLowerCase() == item) {
            //         e.options.hidden = !auth.hasPermi(all)
            //     }
            // });
        });
        return json;
    }
}

function getObjects(obj, key, val, b) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;        
      if (typeof obj[i] == "object") {          
        objects = objects.concat(getObjects(obj[i], key, val,b));
      }
      //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
      else if ((i == key && obj[i] == val) || (i == key && val == "")) {
        //
        obj.options.hidden = b
        objects.push(obj);
      } else if (obj[i] == val && key == "") {
        //only add if the object is not already in the array
        if (objects.lastIndexOf(obj) == -1) {
          obj.options.hidden = b
          objects.push(obj);
        }
      }
    }     
    return objects;
  }
