import BpmnPropertiesPanel from "@/views/workflow/penal/PropertiesPanel.vue";

BpmnPropertiesPanel.install = function (Vue) {
  Vue.component(BpmnPropertiesPanel.name, BpmnPropertiesPanel);
};

export default BpmnPropertiesPanel;
