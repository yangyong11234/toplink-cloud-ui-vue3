import extension from './extension';
"use strict";

export default {
  __init__: ["camundaModdleExtension"],
  camundaModdleExtension: ["type", extension]
};
