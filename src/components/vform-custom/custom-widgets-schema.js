import { dateTableEmits } from "element-plus/es/components/calendar/src/date-table.mjs"

/* --------------------提示信息-------------------- */
export const collapseSchema = {
  type: 'collapse',
  category: 'container',
  icon: 'card',
  widgetList: [],
  options: {
    name: '',
    label: 'collapse',
    hidden: false,
    folded: false,
    customClass: '',
  }
}

/* --------------------查找输入-------------------- */
export const searchInputSchema = {
  type: 'search-input',
  icon: 'search-input',
  formItemFlag: true,
  options: {
    name: '',
    label: '',
    labelAlign: '',
    defaultValue: '',
    placeholder: '',
    columnWidth: '200px',
    size: '',
    labelWidth: null,
    labelHidden: false,
    readonly: false,
    disabled: false,
    hidden: false,
    customClass: '',
  }
}

/* --------------------表格右侧工具栏-------------------- */
export const rightToolbarSchema = {
  type: 'right-toolbar',
  icon: 'system',
  formItemFlag: true,
  options: {
    name: '',
    label: '',
    labelAlign: '',
    defaultValue: '',
    placeholder: '',
    columnWidth: '200px',
    size: '',
    labelWidth: null,
    labelHidden: true,
    readonly: false,
    disabled: false,
    hidden: false,
    customClass: '',
    columns: [],
    // 显隐数据
    value: [],
    // 弹出层标题
    title: "显示/隐藏",
    // 是否显示弹出层
    open: false,
    // 列表组件唯一名称
    tableRefName: 'datatableTable',
    onCreated: '',
    onMounted: '',
    onInput: '',
    onChange: '',
    onClick: "// 在这里处理点击事件，params 参数包含了被点击的数据项的信息\rconsole.log('点击了', params);",
  }
}

/* --------------------导入模版-------------------- */
export const excelImpSchema = {
  type: 'excel-imp',
  icon: 'button',
  formItemFlag: false,
  options: {
    name: 'excelImp',
    label: '导入',
    columnWidth: '200px',
    size: '',
    displayStyle: "inline",
    disabled: false,
    hidden: false,
    type: "info",
    plain: false,
    round: false,
    circle: false,
    icon: "TopLeft",
    dataImp: {//导入模版配置
      dataTableName: "datatableTable",
      allConfig: {
        rowValid: {
          uniqueValid: {
            fieldList: [],
            validTips: ""
          }
        },
        resolverData: [
          { columnId: 1, prop: 'name', label: '姓名', valid: { required: true, fieldLength: 0, dictType: '', replace: '', sole: true, KeyValue: '', unique: false } },
          { columnId: 2, prop: 'date1', label: '日期1', valid: { required: true, fieldLength: 0, dictType: '', replace: '', sole: true, KeyValue: '', unique: false } },
          { columnId: 3, prop: 'date2', label: '日期2', valid: { required: true, fieldLength: 0, dictType: '', replace: '', sole: true, KeyValue: '', unique: false } },
          { columnId: 4, prop: 'date3', label: '日期3', valid: { required: true, fieldLength: 0, dictType: '', replace: '', sole: true, KeyValue: '', unique: false } }
        ]
      }
    },
    //-------------------
    customClass: '',  //自定义css类名
    //-------------------
    onCreated: '',
    onMounted: '',
    onClick: '',
  },
}

/* --------------------导出模版-------------------- */
export const excelExpSchema = {
  type: 'excel-exp',
  icon: 'button',
  formItemFlag: false,
  options: {
    name: 'excelExp',
    label: '导出',
    columnWidth: '200px',
    size: '',
    displayStyle: "inline",
    disabled: false,
    hidden: false,
    type: "warning",
    plain: false,
    round: false,
    circle: false,
    icon: "TopRight",
    dataExp: {//导出模版配置
      dataTableName: "datatableTable",
      resolverData: [
        { columnId: 1, prop: 'name', label: '姓名', describe: "", dictType: "" },
        { columnId: 2, prop: 'date1', label: '日期1', describe: "", dictType: "" },
        { columnId: 3, prop: 'date2', label: '日期2', describe: "", dictType: "" },
        { columnId: 4, prop: 'date3', label: '日期3', describe: "", dictType: "" }
      ]
    },
    //-------------------
    customClass: '',  //自定义css类名
    //-------------------
    onCreated: '',
    onMounted: '',
    onClick: '',
  },
}

/* --------------------柱状图-------------------- */
export const barSchema = {
  type: 'bar',
  icon: 'bar',
  formItemFlag: false, //是否需嵌套于el-form-item
  options: {
    name: 'bar',
    type: 'bar',
    hidden: false,
    theme: {
      themeType: 0,//自定义主题类型0默认主题1自定义主题2动态主题
      themeColor: [],
      themeBgColor: "#FFFFFF",
    },
    onClose: '',
    onClick: "// 在这里处理点击事件，params 参数包含了被点击的数据项的信息\rconsole.log('点击了', params);",
    customClass: '',
    seriesName: "",
    seriesData: [],
    dsEnabled: false, //是否使用数据源数据
    dsName: '',  //数据源名称
    dataSetName: '',  //数据集名称,
    defaultValue: [],
    label: "bar",
    labelKey: "label",
    valueKey: "value",
    optionItems: [],
    echartsOption: {
      darkMode: false,//暗黑模式
      animation: true,//过度动画
      width: "100%",
      height: "300",
      title: {
        text: 'Stacked Area Chart',
        x: 'center',
        y: 'top'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      grid: {
        left: '30',
        right: null,
        top: '50',
        bottom: null,
        containLabel: true
      },
      dataset: {
        source: [
          ['product', '2015', '2016', '2017'],
          ['Matcha Latte', 43.3, 85.8, 93.7],
          ['Milk Tea', 83.1, 73.4, 55.1],
          ['Cheese Cocoa', 86.4, 65.2, 82.5],
          ['Walnut Brownie', 72.4, 53.9, 39.1]
        ]
      },
      xAxis: {
        type: 'category',
        axisLabel: {
          margin: 10, // 增加X轴标签与图表底部的距离
        }
      },
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [{ type: 'bar' }, { type: 'bar' }, { type: 'bar' }],
      legend: {
        show: true,//显示图例
        orient: 'horizontal',//图例方向
        left: 'left',//图例水平样式
        top: 'top',//图例垂直样式
        textStyle: {
          color: "#000000"
        },
      },
      barProps: {
        barWidth: null,
        barMaxWidth: '100%',
        barMinHeight: 10,
        barMinHeight: 0,
        barGap: '30%',//不同类柱间隔
        barCategoryGap: '40%'//同类柱间隔
      },
      dataZoom: [
        {
          show: true,
          type: 'slider', // 滑动条型数据区域缩放组件，显示在图表下方
          xAxisIndex: [0], // 同上，应用于哪个X轴
          start: 0,
          end: 100,
          height: 10,
          bottom: 10,
          showDetail: false, // 是否显示详细的数值信息
          handleSize: '80%', // 滑动条的拖动手柄大小，可设置像素值或百分比
          borderColor: '#909399', // 滑动条边框颜色
          backgroundColor: '#F0F3F6', // 滑动条背景色
          fillerColor: '#C1CFFB', // 选中范围填充色
          emphasis: {
            handleStyle: {
              color: '#A6A6A6', // 拖动手柄鼠标悬停时的颜色
            }
          }
        }
      ],
    }
  }
}

/* --------------------折线图-------------------- */
export const lineSchema = {
  type: 'line',
  icon: 'line',
  formItemFlag: false, //是否需嵌套于el-form-item
  options: {
    name: 'line',
    type: 'line',
    hidden: false,
    theme: {
      themeType: 0,//自定义主题类型0默认主题1自定义主题2动态主题
      themeColor: [],
      themeBgColor: "#FFFFFF",
    },
    onClose: '',
    onClick: "// 在这里处理点击事件，params 参数包含了被点击的数据项的信息\rconsole.log('点击了', params);",
    customClass: '',
    seriesName: "",
    seriesData: [],
    dsEnabled: false, //是否使用数据源数据
    dsName: '',  //数据源名称
    dataSetName: '',  //数据集名称,
    defaultValue: [],
    label: "line",
    labelKey: "label",
    valueKey: "value",
    optionItems: [],
    echartsOption: {
      darkMode: false,//暗黑模式
      width: "100%",
      height: "300",
      title: {
        text: '折线图示例-整改情况',
        x: 'center',
        y: 'top'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#6a7985'
          }
        }
      },
      legend: {
        show: true,//显示图例
        orient: 'horizontal',//图例方向
        left: 'left',//图例水平样式
        top: 'top',//图例垂直样式
        textStyle: {
          color: "#000000"
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        top: '50',
        bottom: null,
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: false,
          data: [
            "04-01", "04-02", "04-03", "04-04", "04-05", "04-06", "04-07", "04-08"
          ]
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: "已整改",
          type: "line",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [
            0, 1, 2, 3, 4, 5, 6, 7
          ]
        },
        {
          name: "未整改",
          type: "line",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [
            7, 6, 5, 4, 3, 2, 1, 0
          ]
        }
      ]
    }
  }
}

/* --------------------饼状图-------------------- */
export const pieSchema = {
  type: 'pie',
  icon: 'pie',
  formItemFlag: false, //是否需嵌套于el-form-item
  options: {
    name: 'pie',
    type: 'pie',
    hidden: false,
    theme: {
      themeType: 0,//自定义主题类型0默认主题1自定义主题2动态主题
      themeColor: [],
      themeBgColor: "#FFFFFF",
    },
    onClose: '',
    onClick: "// 在这里处理点击事件，params 参数包含了被点击的数据项的信息\rconsole.log('点击了', params);",
    customClass: '',
    seriesName: "",
    seriesData: [],
    dsEnabled: false, //是否使用数据源数据
    dsName: '',  //数据源名称
    dataSetName: '',  //数据集名称,
    defaultValue: [],
    label: "pie",
    labelKey: "label",
    valueKey: "value",
    optionItems: [],
    echartsOption: {
      darkMode: false,//暗黑模式
      animation: true,//过度动画
      width: "100%",
      height: "380",
      title: {
        text: '网站占比',
        x: 'center',
        y: 'top'
      },
      tooltip: {
        trigger: 'item',
        formatter: '{b}: {c} ({d}%)',
      },
      legend: {
        show: true,//显示图例
        orient: 'vertical',//图例方向
        left: 'right',//图例水平样式
        top: 'bottom',//图例垂直样式
        textStyle: {
          color: "#000000"
        },
      },
      series: [
        {
          name: '占比',
          type: 'pie',
          radius: ['0%', '70%'],
          center: ['50%', '50%'],
          data: [
            { value: 100, name: '2023年' },
            { value: 80, name: '2022年' },
            { value: 60, name: '2021年' },
            { value: 40, name: '2020年' },
            { value: 20, name: '2019年' }
          ],
          avoidLabelOverlap: true,
          label: {
            position: 'outside', // 强制标签显示在外部
            alignTo: 'labelLine',
            formatter: '{b}: {c} ({d}%)',
            minMargin: 5,
            edgeDistance: 10,
            lineHeight: 15,
            rich: {
              time: {
                fontSize: 10,
                color: '#999'
              }
            }
          },
          itemStyle: {
            borderRadius: 0, // 设置为0表示实心饼图
            borderColor: '#fff', // 设置边框颜色
            borderWidth: 2, // 设置边框宽度
            // label: {
            //   show: true,
            //   position: 'center'
            // },
            // emphasis: {
            //   label: {
            //     show: true,
            //     fontSize: '22',
            //     fontWeight: 'bold'
            //   }
            // },
            // labelLine: {
            //   show: true
            // }
          }
        }
      ]
    }
  }
}

/* --------------------雷达图-------------------- */
export const radarSchema = {
  type: 'radar',
  icon: 'radar',
  formItemFlag: false, //是否需嵌套于el-form-item
  options: {
    name: '',
    type: 'radar',
    hidden: false,
    theme: {
      themeType: 0,//自定义主题类型0默认主题1自定义主题2动态主题
      themeColor: [],
      themeBgColor: "#FFFFFF",
    },
    onClose: '',
    onClick: "// 在这里处理点击事件，params 参数包含了被点击的数据项的信息\rconsole.log('点击了', params);",
    customClass: '',
    seriesName: "",
    seriesData: [],
    dsEnabled: false, //是否使用数据源数据
    dsName: '',  //数据源名称
    dataSetName: '',  //数据集名称,
    defaultValue: [],
    label: "pie",
    labelKey: "label",
    valueKey: "value",
    optionItems: [],
    echartsOption: {
      darkMode: false,//暗黑模式
      animation: true,//过度动画
      width: "100%",
      height: "300",
      title: {
        text: 'Basic Radar Chart',
        x: 'center',
        y: 'top'
      },
      legend: {
        show: true,//显示图例
        orient: 'horizontal',//图例方向
        left: 'left',//图例水平样式
        top: 'top',//图例垂直样式
        textStyle: {
          color: "#000000"
        }
      },
      radar: {
        // shape: 'circle',
        axisName: {
          color: "#000000"
        },
        indicator: [
          { name: 'Sales' },
          { name: 'Administration' },
          { name: 'Information Technology' },
          { name: 'Customer Support' },
          { name: 'Development' },
          { name: 'Marketing' }
        ]
      },
      series: [
        {
          name: '对比',
          type: 'radar',
          data: [
            {
              value: [4200, 3000, 20000, 35000, 50000, 18000],
              name: 'Allocated Budget',
              label: {
                show: true,
                formatter: function (params) {
                  return params.value;
                }
              }
            },
            {
              value: [5000, 14000, 28000, 26000, 42000, 21000],
              name: 'Actual Spending',
              label: {
                show: true,
                formatter: function (params) {
                  return params.value;
                }
              }
            }
          ]
        }
      ]
    }
  }
}

/* --------------------可编辑数据表格-------------------- */
export const editableDataTableSchema = {
  type: 'editable-data-table',
  icon: 'table',
  formItemFlag: false, //是否需嵌套于el-form-item
  options: {
    name: '',
    hidden: false,
    customClass: '',
    seriesName: "",
    seriesData: [],
    dsEnabled: false, //是否使用数据源数据
    dsName: '',  //数据源名称
    dataSetName: '',  //数据集名称,
    defaultValue: [],
    optionItems: [],
    gridOptions:
    {
      "border": true,
      "editConfig": {
        "trigger": "click",
        "mode": "cell"
      },
      "columns": [
        {
          "columnId": 1,
          "type": "seq",
          "width": 70
        },
        {
          "columnId": 2,
          "field": "name",
          "title": "Name",
          "editRender": {
            "name": "input"
          },
          "formatter": "mySwitch"
        },
        {
          "columnId": 3,
          "field": "sex",
          "title": "Sex",
          "editRender": {
            "name": "ElSelect",
            "options": [
              {
                "label": "男",
                "value": "Man"
              },
              {
                "label": "女",
                "value": "Women"
              }
            ]
          }
        },
        {
          "columnId": 4,
          "field": "age",
          "title": "Age",
          "editRender": {
            "name": "input"
          }
        },
        {
          "columnId": 5,
          "field": "flag",
          "title": "开关",
          "width": 200,
          "cellRender": {
            "name": "ElSwitch"
          }
        }
      ],
      "data": [
        { "id": 10001, "name": "Test1", "sex": "Man", "age": 28, "flag": false },
        { "id": 10002, "name": "Test2", "sex": "Women", "age": 22, "flag": false },
        { "id": 10003, "name": "Test3", "sex": "Man", "age": 32, "flag": false },
        { "id": 10004, "name": "Test4", "sex": "Women", "age": 24, "flag": false }
      ],
      "height":"100%",
    },
    onCreated: "//当表格组件创建后触发的事件\n// 格式化开关\nthis.VxeUI.formats.add('mySwitch', {\n  tableCellFormatMethod ({ cellValue }) {\n    return cellValue == 'Test1' ? '测试1-单元格格式化' : cellValue\n  }\n})",
    onMounted: "//当表格组件挂载后触发的事件\n",
    keydownStart: '//当表格被激活且键盘被按下开始时会触发的事件, params 默认值 / 参数:{ $event }\n',
    keydown: '//当表格被激活且键盘被按下时会触发的事件, params 默认值 / 参数:{ $event }\n',
    keydownEnd: '//当表格被激活且键盘被按下结束时会触发的事件, params 默认值 / 参数:{ $event }\n',
    currentChange: '//只对 row-config.isCurrent 有效，当手动选中行并且值发生改变时触发的事件, params 默认值 / 参数:{ newValue, oldValue, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    radioChange: '//只对 type=radio 有效，当手动勾选并且值发生改变时触发的事件, params 默认值 / 参数:{ newValue, oldValue, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    checkboxChange: '//只对 type=checkbox 有效，当手动勾选并且值发生改变时触发的事件, params 默认值 / 参数:{ checked, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    checkboxAll: '//只对 type=checkbox 有效，当手动勾选全选时触发的事件, params 默认值 / 参数:{ checked, $event }\n',
    checkboxRangeStart: '//只对 checkbox-config.range 有效，当鼠标范围选择开始时会触发的事件, params 默认值 / 参数:{ $event }\n',
    checkboxRangeChange: '//只对 checkbox-config.range 有效，当鼠标范围选择内的行数发生变化时会触发的事件, params 默认值 / 参数:{ $event }\n',
    checkboxRangeEnd: '//只对 checkbox-config.range 有效，当鼠标范围选择结束时会触发的事件, params 默认值 / 参数:{ $event }\n',
    cellClick: '//单元格被点击时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, triggerRadio, triggerCheckbox, triggerTreeNode, triggerExpandNode, $event }\n',
    cellDblclick: '//单元格被双击时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    cellMenu: '//只对 menu-config 配置时有效，单元格被鼠标右键时触发该事件, params 默认值 / 参数:{ type, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    cellMouseenter: '//只对 tooltip-config 配置时有效，当鼠标移动到单元格时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    cellMouseleave: '// 只对 tooltip-config 配置时有效，当鼠标移开单元格时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    cellDeleteValue: '// 只对 keyboard-config.isDel 有效，当按下删除键指定清空单元格值时会触发该事件, params 默认值 / 参数:{ row, rowIndex, column, columnIndex, activeArea, cellAreas, $event }\n',
    headerCellClick: '// 表头单元格被点击时会触发该事件, params 默认值 / 参数:{ $rowIndex, column, columnIndex, $columnIndex, triggerResizable, triggerSort, triggerFilter, $event }\n',
    headerCellDblclick: '// 表头单元格被双击时会触发该事件, params 默认值 / 参数:{ $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    headerCellMenu: '// 只对 menu-config 配置时有效，表头单元格被鼠标右键时触发该事件, params 默认值 / 参数:{ type, column, columnIndex, $event }\n',
    footerCellClick: '// 表尾单元格被点击时会触发该事件, params 默认值 / 参数:{ items, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    footerCellDblclick: '// 表尾单元格被双击时会触发该事件, params 默认值 / 参数:{ items, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    footerCellMenu: '// 只对 menu-config 配置时有效，表尾单元格被鼠标右键时触发该事件, params 默认值 / 参数:{ type, column, columnIndex, $event }\n',
    clearMerge: '// 当用户点击取消所有临时合并时会触发该事件, params 默认值 / 参数:{ mergeCells, mergeFooterItems, $event }\n',
    sortChange: '// 当排序条件发生变化时会触发该事件, params 默认值 / 参数:{ column, field, order, sortBy, sortList, $event }\n',
    clearSort: '// 当用户点击清除所有排序时会触发该事件, params 默认值 / 参数:{ sortList, $event }\n',
    filterChange: '// 当筛选条件发生变化时会触发该事件, params 默认值 / 参数:{ column, field, values, datas, filterList, $event }\n',
    filterVisible: '// 当筛选面板被触发时会触发该事件, params 默认值 / 参数:{ column, field, visible, filterList, $event }\n',
    clearFilter: '// 当用户点击清除所有筛选条件时会触发该事件, params 默认值 / 参数:{ filterList, $event }\n',
    resizableChange: '// 当列宽拖动发生变化时会触发该事件, params 默认值 / 参数:{ $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    toggleRowExpand: '// 当行展开或收起时会触发该事件, params 默认值 / 参数:{ expanded, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex, $event }\n',
    toggleTreeExpand: '// 当树节点展开或收起时会触发该事件, params 默认值 / 参数:{ expanded, row, column, columnIndex, $columnIndex, $event }\n',
    menuClick: '// 只对 menu-config 配置时有效，当点击右键菜单时会触发该事件, 默认值 / 参数:{ menu, type, row, rowIndex, column, columnIndex, $event }\n',
    cellSelected: '// 只对 mouse-config.selected 配置时有效，单元格被选中时会触发该事件, 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex }\n',
    editClosed: '// 只对 edit-config 配置时有效，单元格编辑状态下被关闭时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex }\n',
    editActivated: '// 只对 edit-config 配置时有效，单元格被激活编辑时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex }\n',
    editDisabled: '// 只对 edit-config 配置时有效，当单元格激活时如果是禁用状态时会触发该事件, params 默认值 / 参数:{ row, rowIndex, $rowIndex, column, columnIndex, $columnIndex }\n',
    validError: '// 只对 edit-rules 配置时有效，当数据校验不通过时会触发该事件, params 默认值 / 参数:{ rule, row, rowIndex, $rowIndex, column, columnIndex, $columnIndex }\n',
    scroll: '// 表格滚动时会触发该事件, params 默认值 / 参数:{ type, scrollTop, scrollLeft, scrollHeight, scrollWidth, bodyWidth, bodyHeight, isX, isY, $event }\n',
    custom: '// 如果与工具栏关联，在自定义列按钮被手动点击后会触发该事件, params 默认值 / 参数:{ type, $event }\n',
    pageChange: '// 只对 pager-config 配置时有效，分页发生改变时会触发该事件, params 默认值 / 参数:{ type, currentPage, pageSize, $event }\n',
    formSubmit: '// 只对 form-config 配置时有效，表单提交时会触发该事件, params 默认值 / 参数:{ data, $event }\n',
    formSubmitInvalid: '// 只对 form-config 配置时有效，表单提交时如果校验不通过会触发该事件, params 默认值 / 参数:{ data, errMap, $event }\n',
    formReset: '// 只对 form-config 配置时有效，表单重置时会触发该事件, params 默认值 / 参数:{ data, $event }\n',
    formCollapse: '// 只对 form-config 配置时有效，当折叠按钮被手动点击时会触发该事件, params 默认值 / 参数:{ status, data, $event }\n',
    proxyQuery: '// 只对 proxy-config.ajax.query 配置时有效，当手动点击查询时会触发该事件, params 默认值 / 参数:{ status, isReload, isInited }\n',
    proxyDelete: '// 只对 proxy-config.ajax.delete 配置时有效，当手动点击删除时会触发该事件, params 默认值 / 参数:{ status }\n',
    proxySave: '// 只对 proxy-config.ajax.save 配置时有效，当手动点击保存时会触发该事件, params 默认值 / 参数:{ status }\n',
    toolbarButtonClick: '// 只对 toolbar.buttons 配置时有效，当左侧按钮被点击时会后触发该事件, params 默认值 / 参数:{ code, button, $event }\n',
    toolbarToolClick: '// 只对 toolbar.tools 配置时有效，当右侧工具被点击时会后触发该事件, params 默认值 / 参数:{ code, tool, $event }\n',
    zoom: '// 当最大化或还原操作被手动点击时会后触发该事件, params 默认值 / 参数:{ type, $event }\n'
  }
}
