export default {
  extension: {
    widgetLabel: {
      'search-input': 'Search Input',
      'right-toolbar': 'Table Right Toolbar',
      'excel-imp': 'Import Template',
      'bar': 'Bar Chart'
    },

    setting: {
      //
    },

  }
}
