export default {
  extension: {
    widgetLabel: {
      'search-input': '查找输入',
      'right-toolbar': '表格右侧工具栏',
      'excel-imp': '导入模版',
      'excel-exp': '导出模版',
      'bar': '柱状图',
      'pie': '饼状图',
      'line': '折线图',
      'capsule': '胶囊图',
      'ring': '动态环图',
      'radar': '雷达图',
      'editable-data-table': '高级数据表格'
    },

    setting: {
      titleText: '标题',
      xAxisData:'X轴数据',
      yAxisData: 'Y轴数据',
      seriesName: '系列名称',
      seriesData: '系列数据',
      pieOption:'饼状图属性',
      lineOption:'折线图属性',
      barOption:'柱状图属性',
      capsuleOption:'胶囊图属性',
      ringOption:'动态环图属性',
      radarOption:'雷达图属性',
      digitalOption:'数字看板属性',
      loadingOption:'加载动画属性',
      borderBoxOption:'边框装饰属性',
      borderBoxName:'选择边框装饰',
      pieStyle:'饼图样式',
      titleXStyle:'标题水平',
      titleYStyle:'标题垂直',
    },

  }
}
