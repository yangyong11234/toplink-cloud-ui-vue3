import { searchInputSchema, rightToolbarSchema, excelExpSchema, excelImpSchema, barSchema, lineSchema, pieSchema, radarSchema, editableDataTableSchema } from "@/components/vform-custom/custom-widgets-schema"
import { registerFieldWidgets } from "@/components/vform-custom/field-widgets";
import rightToolbarEditor from '@/components/vform-custom/property-editor/right-toolbar-editor.vue';
import excelImpEditor from '@/components/vform-custom/property-editor/excel-imp-editor.vue';
import excelExpEditor from '@/components/vform-custom/property-editor/excel-exp-editor.vue';
import echartsOptionEditor from '@/components/vform-custom/property-editor/echarts/option-editor.vue';
import editableDataTableEditor from '@/components/vform-custom/property-editor/editable-data-table-editor.vue'

import zhCustomLang from '@/components/vform-custom/lang/zh-CN_custom'
import enCustomLang from '@/components/vform-custom/lang/en-US_custom'

import VForm3 from '@/../lib/vform/designer.umd.js'
// svg图标
import 'virtual:svg-icons-register'

const { addBasicFieldSchema, addAdvancedFieldSchema, addCustomWidgetSchema, addZHExtensionLang,
  addENExtensionLang, PERegister, PEFactory } = VForm3.VFormSDK

export const loadCustomWidgets = (app) => {
  //加载语言文件
  addZHExtensionLang(zhCustomLang)
  addENExtensionLang(enCustomLang)

  //注册扩展字段组件
  registerFieldWidgets(app)

  //注册容器组件（暂无）

  //注册属性编辑器
  //事件属性编辑器 (实例名称,字段名称,组件名称,组件)
  //PERegister.registerEPEditor(app, 'onRecordSelected', 'onRecordSelected-editor', onRecordSelectedEditor)
  PERegister.registerEPEditor(app, 'keydownStart', 'keydownStart-editor',
    PEFactory.createEventHandlerEditor('keydownStart', ['$event']))
  PERegister.registerEPEditor(app, 'keydown', 'keydown-editor',
    PEFactory.createEventHandlerEditor('keydown', ['event']))
  PERegister.registerEPEditor(app, 'keydownEnd', 'keydownEnd-editor',
    PEFactory.createEventHandlerEditor('keydownEnd', ['$event']));
  PERegister.registerEPEditor(app, 'currentChange', 'currentChange-editor',
    PEFactory.createEventHandlerEditor('currentChange', ['newValue', 'oldValue', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'radioChange', 'radioChange-editor',
    PEFactory.createEventHandlerEditor('radioChange', ['newValue', 'oldValue', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'checkboxChange', 'checkboxChange-editor',
    PEFactory.createEventHandlerEditor('checkboxChange', ['checked', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'checkboxAll', 'checkboxAll-editor',
    PEFactory.createEventHandlerEditor('checkboxAll', ['checked', '$event']));
  PERegister.registerEPEditor(app, 'checkboxRangeStart', 'checkboxRangeStart-editor',
    PEFactory.createEventHandlerEditor('checkboxRangeStart', ['$event']));
  PERegister.registerEPEditor(app, 'checkboxRangeChange', 'checkboxRangeChange-editor',
    PEFactory.createEventHandlerEditor('checkboxRangeChange', ['$event']));
  PERegister.registerEPEditor(app, 'checkboxRangeEnd', 'checkboxRangeEnd-editor',
    PEFactory.createEventHandlerEditor('checkboxRangeEnd', ['$event']));
  PERegister.registerEPEditor(app, 'cellClick', 'cellClick-editor',
    PEFactory.createEventHandlerEditor('cellClick', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', 'triggerRadio', 'triggerCheckbox', 'triggerTreeNode', 'triggerExpandNode', '$event']));
  PERegister.registerEPEditor(app, 'cellDblclick', 'cellDblclick-editor',
    PEFactory.createEventHandlerEditor('cellDblclick', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'cellMenu', 'cellMenu-editor',
    PEFactory.createEventHandlerEditor('cellMenu', ['type', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'cellMouseenter', 'cellMouseenter-editor',
    PEFactory.createEventHandlerEditor('cellMouseenter', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'cellMouseleave', 'cellMouseleave-editor',
    PEFactory.createEventHandlerEditor('cellMouseleave', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'cellDeleteValue', 'cellDeleteValue-editor',
    PEFactory.createEventHandlerEditor('cellDeleteValue', ['row', 'rowIndex', 'column', 'columnIndex', 'activeArea', 'cellAreas', '$event']));
  PERegister.registerEPEditor(app, 'headerCellClick', 'headerCellClick-editor',
    PEFactory.createEventHandlerEditor('headerCellClick', ['$rowIndex', 'column', 'columnIndex', '$columnIndex', 'triggerResizable', 'triggerSort', 'triggerFilter', '$event']));
  PERegister.registerEPEditor(app, 'headerCellDblclick', 'headerCellDblclick-editor',
    PEFactory.createEventHandlerEditor('headerCellDblclick', ['$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'headerCellMenu', 'headerCellMenu-editor',
    PEFactory.createEventHandlerEditor('headerCellMenu', ['type', 'column', 'columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'footerCellClick', 'footerCellClick-editor',
    PEFactory.createEventHandlerEditor('footerCellClick', ['items', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'footerCellDblclick', 'footerCellDblclick-editor',
    PEFactory.createEventHandlerEditor('footerCellDblclick', ['items', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'footerCellMenu', 'footerCellMenu-editor',
    PEFactory.createEventHandlerEditor('footerCellMenu', ['type', 'column', 'columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'clearMerge', 'clearMerge-editor',
    PEFactory.createEventHandlerEditor('clearMerge', ['mergeCells', 'mergeFooterItems', '$event']));
  PERegister.registerEPEditor(app, 'sortChange', 'sortChange-editor',
    PEFactory.createEventHandlerEditor('sortChange', ['column', 'field', 'order', 'sortBy', 'sortList', '$event']));
  PERegister.registerEPEditor(app, 'clearSort', 'clearSort-editor',
    PEFactory.createEventHandlerEditor('clearSort', ['sortList', '$event']));
  PERegister.registerEPEditor(app, 'filterChange', 'filterChange-editor',
    PEFactory.createEventHandlerEditor('filterChange', ['column', 'field', 'values', 'datas', 'filterList', '$event']));
  PERegister.registerEPEditor(app, 'filterVisible', 'filterVisible-editor',
    PEFactory.createEventHandlerEditor('filterVisible', ['column', 'field', 'visible', 'filterList', '$event']));
  PERegister.registerEPEditor(app, 'clearFilter', 'clearFilter-editor',
    PEFactory.createEventHandlerEditor('clearFilter', ['filterList', '$event']));
  PERegister.registerEPEditor(app, 'resizableChange', 'resizableChange-editor',
    PEFactory.createEventHandlerEditor('resizableChange', ['$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'toggleRowExpand', 'toggleRowExpand-editor',
    PEFactory.createEventHandlerEditor('toggleRowExpand', ['expanded', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'toggleTreeExpand', 'toggleTreeExpand-editor',
    PEFactory.createEventHandlerEditor('toggleTreeExpand', ['expanded', 'row', 'column', 'columnIndex', '$columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'menuClick', 'menuClick-editor',
    PEFactory.createEventHandlerEditor('menuClick', ['menu', 'type', 'row', 'rowIndex', 'column', 'columnIndex', '$event']));
  PERegister.registerEPEditor(app, 'cellSelected', 'cellSelected-editor',
    PEFactory.createEventHandlerEditor('cellSelected', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex']));
  PERegister.registerEPEditor(app, 'editClosed', 'editClosed-editor',
    PEFactory.createEventHandlerEditor('editClosed', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex']));
  PERegister.registerEPEditor(app, 'editActivated', 'editActivated-editor',
    PEFactory.createEventHandlerEditor('editActivated', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex']));
  PERegister.registerEPEditor(app, 'editDisabled', 'editDisabled-editor',
    PEFactory.createEventHandlerEditor('editDisabled', ['row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex']));
  PERegister.registerEPEditor(app, 'validError', 'validError-editor',
    PEFactory.createEventHandlerEditor('validError', ['rule', 'row', 'rowIndex', '$rowIndex', 'column', 'columnIndex', '$columnIndex']));
  PERegister.registerEPEditor(app, 'scroll', 'scroll-editor',
    PEFactory.createEventHandlerEditor('scroll', ['type', 'scrollTop', 'scrollLeft', 'scrollHeight', 'scrollWidth', 'bodyWidth', 'bodyHeight', 'isX', 'isY', '$event']));
  PERegister.registerEPEditor(app, 'custom', 'custom-editor',
    PEFactory.createEventHandlerEditor('custom', ['type', '$event']));
  PERegister.registerEPEditor(app, 'pageChange', 'pageChange-editor',
    PEFactory.createEventHandlerEditor('pageChange', ['type', 'currentPage', 'pageSize', '$event']));
  PERegister.registerEPEditor(app, 'formSubmit', 'formSubmit-editor',
    PEFactory.createEventHandlerEditor('formSubmit', ['data', '$event']));
  PERegister.registerEPEditor(app, 'formSubmitInvalid', 'formSubmitInvalid-editor',
    PEFactory.createEventHandlerEditor('formSubmitInvalid', ['data', 'errMap', '$event']));
  PERegister.registerEPEditor(app, 'formReset', 'formReset-editor',
    PEFactory.createEventHandlerEditor('formReset', ['data', '$event']));
  PERegister.registerEPEditor(app, 'formCollapse', 'formCollapse-editor',
    PEFactory.createEventHandlerEditor('formCollapse', ['status', 'data', '$event']));
  PERegister.registerEPEditor(app, 'proxyQuery', 'proxyQuery-editor',
    PEFactory.createEventHandlerEditor('proxyQuery', ['status', 'isReload', 'isInited']));
  PERegister.registerEPEditor(app, 'proxyDelete', 'proxyDelete-editor',
    PEFactory.createEventHandlerEditor('proxyDelete', ['status']));
  PERegister.registerEPEditor(app, 'proxySave', 'proxySave-editor',
    PEFactory.createEventHandlerEditor('proxySave', ['status']));
  PERegister.registerEPEditor(app, 'toolbarButtonClick', 'toolbarButtonClick-editor',
    PEFactory.createEventHandlerEditor('toolbarButtonClick', ['code', 'button', '$event']));
  PERegister.registerEPEditor(app, 'toolbarToolClick', 'toolbarToolClick-editor',
    PEFactory.createEventHandlerEditor('toolbarToolClick', ['code', 'tool', '$event']));
  PERegister.registerEPEditor(app, 'zoom', 'zoom-editor',
    PEFactory.createEventHandlerEditor('zoom', ['type', '$event']));
  //常见属性编辑器 (实例名称,字段名称,组件名称,组件)
  PERegister.registerCPEditor(app, 'tableRefName', 'right-toolbar-editor', rightToolbarEditor)
  PERegister.registerCPEditor(app, 'dataImp', 'excel-imp-editor', excelImpEditor)
  PERegister.registerCPEditor(app, 'dataExp', 'excel-exp-editor', excelExpEditor)
  PERegister.registerCPEditor(app, 'echartsOption', 'echarts-editor', echartsOptionEditor)
  PERegister.registerCPEditor(app, 'gridOptions', 'editable-data-table-editor', editableDataTableEditor)

  /** 
   * 添加到设计器组件库,加载组件Json Schema
   * addBasicFieldSchema() 添加到基础字段组件库
   * addAdvancedFieldSchema() 添加到高级字段组件库
   * addCustomWidgetSchema() 添加到扩展字段组件库 
  */
  addAdvancedFieldSchema(searchInputSchema)  //加载组件Json Schema
  addAdvancedFieldSchema(rightToolbarSchema) //列表右侧工具栏
  addCustomWidgetSchema(excelImpSchema) //导入按钮模版
  addCustomWidgetSchema(excelExpSchema) //导出按钮模版
  addCustomWidgetSchema(barSchema)  //柱状图
  addCustomWidgetSchema(lineSchema) //折线图
  addCustomWidgetSchema(pieSchema)  //饼状图
  addCustomWidgetSchema(radarSchema) //雷达图
  addAdvancedFieldSchema(editableDataTableSchema) //可编辑数据表格
}
