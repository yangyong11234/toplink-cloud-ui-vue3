export const registerFieldWidgets = (app) => {
	//注册当前目录下的组件
	const modules = import.meta.glob('./*.vue', { eager: true })
	for (const path in modules) {
		let cname = modules[path].default.name
		app.component(cname, modules[path].default)
	}
	//注册当前目录下在任意目录中的组件
	const pathModules = import.meta.glob('./*/*.vue', { eager: true })
	for (const paths in pathModules) {
		let cname = pathModules[paths].default.name
		app.component(cname, pathModules[paths].default)
	}
}