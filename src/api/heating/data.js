import request from '@/utils/request'
import {download} from '@/utils/request'

// 查询列表
export function listInfo(query, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName + '/list',
        method: 'get',
        params: query
    })
}

// 查询详细
export function getInfo(id, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName +'/'+  id,
        method: 'get'
    })
}

// 新增
export function addInfo(data, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName ,
        method: 'post',
        data: data
    })
}

// 修改
export function updateInfo(data, businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName ,
        method: 'put',
        data: data
    })
}

// 删除
export function delInfo(id, businessName,moduleName) {
    console.log(id,name)
    return request({
        url:'/'+moduleName+'/' + businessName +'/'+ id,
        method: 'delete'
    })
}

// 导入
export function exportInfo(params, businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName +  '/export',
        method: 'post',
        data: params
    })
}

// 导出
export function exportInfoTemplate(queryParams, businessName,moduleName){
    download('/'+moduleName+'/' + businessName +  '/exportTemplate', {
        ...queryParams
    }, `${businessName}_${new Date().getTime()}.xlsx`)
}
