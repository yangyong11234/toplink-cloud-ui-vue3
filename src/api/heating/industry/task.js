import request, { download } from '@/utils/request'

// 查询业务受理列表（工单管理）
export function managerListTask(query) {
  return request({
    url: '/heating/task/managerList',
    method: 'get',
    params: query
  })
}
// 查询业务受理列表（暂存工单）
export function temporaryListTask(query) {
  return request({
    url: '/heating/task/temporaryList',
    method: 'get',
    params: query
  })
}
// 查询业务受理列表（历史工单）
export function historyListTask(query) {
  return request({
    url: '/heating/task/historyList',
    method: 'get',
    params: query
  })
}
// 查询业务受理列表（我的待处理）
export function pendingListTask(query) {
  return request({
    url: '/heating/task/pendingList',
    method: 'get',
    params: query
  })
}
// 查询业务受理列表（审核归档）
export function placeAuditListTask(query) {
  return request({
    url: '/heating/task/placeAuditList',
    method: 'get',
    params: query
  })
}
// 办理跳转后修改数据的接口
export function updateTaskDept(data) {
  return request({
    url: '/heating/task/updateTaskDept',
    method: 'post',
    data: data
  })
}
// 市级抽查回访POST接口
export function spotCheck(data) {
  return request({
    url: '/heating/task/spotCheck',
    method: 'post',
    data: data
  })
}
// 判断延迟申请按钮是否显示的接口
export function isOperatorDelayWorkFlow(data) {
  return request({
    url: '/heating/task/isOperatorDelayWorkFlow?taskId=' + data,
    method: 'get'
  })
}
export function existImportantUser(phone) {
  return request({
    url: '/heating/task/existImportantUser?phone=' + phone,
    method: 'get'
  })
}

//查询已删除的业务受理列表
export function recycleList(query) {
  return request({
    url: '/heating/task/recycleList',
    method: 'get',
    params: query
  })
}

//分页查询延时批复列表
export function delayAuditList(query) {
  return request({
    url: '/heating/task/delayAuditList',
    method: 'get',
    params: query
  })
}

// 查询业务受理详细
export function getTask(id) {
  return request({
    url: '/heating/task/' + id,
    method: 'get'
  })
}

// 新增业务受理
export function addTask(data) {
  return request({
    url: '/heating/task',
    method: 'post',
    data: data
  })
}

// 修改业务受理
export function updateTask(data) {
  return request({
    url: '/heating/task',
    method: 'put',
    data: data
  })
}

// 删除业务受理
export function delTask(id) {
  return request({
    url: '/heating/task/' + id,
    method: 'delete'
  })
}
//导出工单管理列表
export function exportManagerList(queryParams) {
  return request({
    url: '/heating/task/exportManagerList',
    method: 'post',
    data: queryParams
  })
}
//导出暂存管理列表
export function exportTemporaryList(queryParams) {
  return request({
    url: '/heating/task/exportTemporaryList',
    method: 'post',
    data: queryParams
  })
}

export function exportDelayAuditList(queryParams) {
  return request({
    url: '/heating/task/exportDelayAuditList',
    method: 'post',
    data: queryParams
  })
}

export function exportPlaceAuditList(queryParams) {
  return request({
    url: '/heating/task/exportPlaceAuditList',
    method: 'post',
    data: queryParams
  })
}

export function exportHistoryList(queryParams) {
  return request({
    url: '/heating/task/exportHistoryList',
    method: 'post',
    data: queryParams
  })
}

export function exportPendingList(queryParams) {
  return request({
    url: '/heating/task/exportPendingList',
    method: 'post',
    data: queryParams
  })
}

export function exportTaskTemplate(queryParams) {
  download('heating/task/exportTemplate', {
    ...queryParams
  }, `task_${new Date().getTime()}.xlsx`)
}

export function exportTask(queryParams) {
  return request({
    url: '/heating/task/exportRecycleList',
    method: 'post',
    data: queryParams
  })
}

export function batchSaveImportAntUser(data) {
  return request({
    url: '/heating/task/batchSaveImportAntUser',
    method: 'post',
    data: data
  })
}

// 点击table行时，给出参数区分时打开drawer还是跳转页面
export function getWorkFlowTaskId(id) {
  return request({
    url: '/heating/task/getWorkFlowTaskId?procInsId=' + id,
    method: 'GET'
  })
}
// 系统根据小区名称，自动查询历史工单，推荐该小区所属的供热单位，推荐值允许修改。
export function queryDeptByEstate(data) {
  return request({
    url: '/heating/task/queryDeptByEstate?estate=' + data,
    method: 'GET'
  })
}
// 供热单位电话根据选择的供热单位自动回显
export function queryHeatingPhone(data) {
  return request({
    url: '/heating/task/queryHeatingPhone?deptId=' + data,
    method: 'GET'
  })
}