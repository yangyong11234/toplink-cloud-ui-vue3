import request from '@/utils/request'
import {download} from '@/utils/request'

// 查询单设计器-数据源管理列表
export function listDataSource(query) {
    return request({
        url: '/designer/DataSource/list',
        method: 'get',
        params: query
    })
}

// 查询单设计器-数据源管理详细
export function getDataSource(id) {
    return request({
        url: '/designer/DataSource/' + id,
        method: 'get'
    })
}

// 新增单设计器-数据源管理
export function addDataSource(data) {
    return request({
        url: '/designer/DataSource',
        method: 'post',
        data: data
    })
}

// 修改单设计器-数据源管理
export function updateDataSource(data) {
    return request({
        url: '/designer/DataSource',
        method: 'put',
        data: data
    })
}

// 删除单设计器-数据源管理
export function delDataSource(id) {
    return request({
        url: '/designer/DataSource/' + id,
        method: 'delete'
    })
}

export function exportDataSource(queryParams) {
    download('designer/DataSource/export', {
        ...queryParams
    }, `dataSource_${new Date().getTime()}.xlsx`)
}


export function exportDataSourceTemplate(queryParams){
    download('designer/DataSource/exportTemplate', {
        ...queryParams
    }, `dataSource_${new Date().getTime()}.xlsx`)
}

// 测试连接池是否可用
export function testConnection(data) {
    return request({
        url: '/designer/DataSource/testConnection',
        method: 'post',
        data,
    })
}