import request from '@/utils/request'
import { download } from '@/utils/request'

export function pageExecute(data) {
    return request({
        url: '/designer/sqlExec/pageExecute',
        method: 'post',
        data: data
    })
}
export function pageSumExecute(data) {
    return request({
        url: '/designer/sqlExec/pageSumExecute',
        method: 'post',
        data: data
    })
}
export function listExecute(data) {
    return request({
        url: '/designer/sqlExec/listExecute',
        method: 'post',
        data: data
    })
}
export function exportListExecute(data) {
    return request({
        url: '/designer/sqlExec/exportListExecute ',
        method: 'post',
        data: data
    })
}
export function tabColTotal(data) {
    return request({
        url: '/designer/sqlExec/tabColTotal',
        method: 'post',
        data: data
    })
}
// 不带分页查询
export function dynListExecute(data) {
    return request({
        url: '/designer/dynSqlExec/listExecute',
        method: 'post',
        data: data
    })
}
// 带分页查询
export function dynPageExecute(data) {
    return request({
        url: '/designer/dynSqlExec/pageExecute',
        method: 'post',
        data: data
    })
}
