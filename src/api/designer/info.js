import request from '@/utils/request'

// 查询单设计器-主列表
export function listDesignerInfo(query) {
  return request({
      url: '/designer/formInfo/list',
      method: 'get',
      params: query
  })
}


// 获取页面列表
export function listForm(query) {
  return request({
      url: '/designer/formInfo/list',
      method: 'get',
      params: query
  })
}

// 保存页面JSON
export function saveVFormJson(query) {
  return request({
    url: '/designer/formInfo/save',
    method: 'post',
    data: query
  })
}

// 读取页面Json
export function getData(id) {
  return request({
      url: '/designer/formInfo/' + id,
      method: 'get'
  })
}

// 修改页面-基础信息
export function update(data) {
  return request({
      url: '/designer/formInfo',
      method: 'put',
      data: data
  })
}

//锁定页面
export function lock(formId) {
  return request({
    url: '/designer/formInfo/lock/' + formId,
    method: 'get',
  })
}

//解锁页面
export function unlock(formId) {
  return request({
    url: '/designer/formInfo/unlock/' + formId,
    method: 'get',
  })
}
