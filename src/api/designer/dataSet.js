import request from '@/utils/request'
import { download } from '@/utils/request'

// 查询-数据集管理列表
export function listDataSet(query) {
    return request({
        url: '/designer/dataSet/list',
        method: 'get',
        params: query
    })
}

// 查询-数据集管理详细
export function getDataSet(id) {
    return request({
        url: '/designer/dataSet/' + id,
        method: 'get'
    })
}

// 新增-数据集管理
export function addDataSet(data) {
    return request({
        url: '/designer/dataSet',
        method: 'post',
        data: data
    })
}

// 修改-数据集管理
export function updateDataSet(data) {
    return request({
        url: '/designer/dataSet',
        method: 'put',
        data: data
    })
}

// 删除-数据集管理
export function delDataSet(id) {
    return request({
        url: '/designer/dataSet/' + id,
        method: 'delete'
    })
}

//锁定-数据集管理
export function lockDataSet(formId) {
    return request({
        url: '/designer/dataSet/lock/' + formId,
        method: 'get',
    })
}

//解锁-数据集管理
export function unlockDataSet(formId) {
    return request({
        url: '/designer/dataSet/unlock/' + formId,
        method: 'get',
    })
}


export function exportDataSet(queryParams) {
    download('designer/dataSet/export', {
        ...queryParams
    }, `dataSet_${new Date().getTime()}.xlsx`)
}


export function exportDataSetTemplate(queryParams) {
    download('designer/dataSet/exportTemplate', {
        ...queryParams
    }, `dataSet_${new Date().getTime()}.xlsx`)
}
