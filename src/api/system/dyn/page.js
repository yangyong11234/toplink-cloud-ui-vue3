import request, {download} from '@/utils/request'

// 查询动态页面配置主列表
export function listPage(query) {
  return request({
    url: '/system/dyn/page/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面配置主详细
export function getPage(id) {
  return request({
    url: '/system/dyn/page/' + id,
    method: 'get'
  })
}

// 新增动态页面配置主
export function addPage(data) {
  return request({
    url: '/system/dyn/page',
    method: 'post',
    data: data
  })
}

// 修改动态页面配置主
export function updatePage(data) {
  return request({
    url: '/system/dyn/page',
    method: 'put',
    data: data
  })
}

// 删除动态页面配置主
export function delPage(id) {
  return request({
    url: '/system/dyn/page/' + id,
    method: 'delete'
  })
}

export function exportPage(queryParams) {
  download('system/dyn/page/export', {
    ...queryParams
  }, `page_${new Date().getTime()}.xlsx`)
}

export function exportPageTemplate(queryParams) {
  download('system/dyn/page/exportTemplate', {
    ...queryParams
  }, `page_${new Date().getTime()}.xlsx`)
}

/**
 * 用于根据从数据库中配置的url、请求方式、查询条件到后台中进行查询
 * 公共的请求后台方法
 */
export function sendPublic(dynUrl, dynMethod, dynData) {
  // 方法类型为get
  if ('get' == dynMethod) {
    return request({
      url: dynUrl + '?' + dynData,
      method: dynMethod
    })
    // 方法类型为post
  } else if ('post' == dynMethod) {
    return request({
      url: dynUrl,
      method: dynMethod,
      data: dynData
    })
    // 方法类型为put
  } else if ('put' === dynMethod) {
    return request({
      url: dynUrl,
      method: dynMethod,
      data: dynData
    })
    // 方法类型为delete
  } else if ('delete' == dynMethod) {
    return request({
      url: dynUrl + '?' + dynData,
      method: dynMethod
    })
  }
}

// 以上方法在一些端点方法调用的时候会出现bug 以下是 bug url；
// http://localhost/dev-api/null?null
// export function sendPublic(dynUrl, dynMethod, dynData) {
//   const config = {
//     url: dynUrl,
//     method: dynMethod,
//     params: ['get', 'delete'].includes(dynMethod) ? dynData : undefined,
//     data: ['post', 'put'].includes(dynMethod) ? dynData : undefined
//   };
//   return request(config);
// }
