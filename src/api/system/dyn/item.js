import request, {download} from '@/utils/request'

// 查询动态页面table合并列表
export function listItem(query) {
  return request({
    url: '/system/dyn/page/tab/item/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面table合并详细
export function getItem(id) {
  return request({
    url: '/system/dyn/page/tab/item/' + id,
    method: 'get'
  })
}

// 新增动态页面table合并
export function addItem(data) {
  return request({
    url: '/system/dyn/page/tab/item',
    method: 'post',
    data: data
  })
}

// 修改动态页面table合并
export function updateItem(data) {
  return request({
    url: '/system/dyn/page/tab/item',
    method: 'put',
    data: data
  })
}

// 删除动态页面table合并
export function delItem(id) {
  return request({
    url: '/system/dyn/page/tab/item/' + id,
    method: 'delete'
  })
}

export function exportItem(queryParams) {
  download('system/dyn/page/tab/item/export', {
    ...queryParams
  }, `item_${new Date().getTime()}.xlsx`)
}

export function exportItemTemplate(queryParams) {
  download('system/dyn/page/tab/item/exportTemplate', {
    ...queryParams
  }, `item_${new Date().getTime()}.xlsx`)
}

