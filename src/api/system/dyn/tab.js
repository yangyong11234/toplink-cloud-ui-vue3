import request, {download} from '@/utils/request'

// 查询动态页面table配置列表
export function listTab(query) {
  return request({
    url: '/system/dyn/page/tab/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面table配置详细
export function getTab(id) {
  return request({
    url: '/system/dyn/page/tab/' + id,
    method: 'get'
  })
}

// 新增动态页面table配置
export function addTab(data) {
  return request({
    url: '/system/dyn/page/tab',
    method: 'post',
    data: data
  })
}

// 修改动态页面table配置
export function updateTab(data) {
  return request({
    url: '/system/dyn/page/tab',
    method: 'put',
    data: data
  })
}

// 修改动态页面table配置排序
export function updateSort(data) {
  return request({
    url: '/system/dyn/page/tab/updateSort',
    method: 'put',
    data: data
  })
}

// 删除动态页面table配置
export function delTab(id) {
  return request({
    url: '/system/dyn/page/tab/' + id,
    method: 'delete'
  })
}

export function exportTab(queryParams) {
  download('system/dyn/page/tab/export', {
    ...queryParams
  }, `tab_${new Date().getTime()}.xlsx`)
}

export function exportTabTemplate(queryParams) {
  download('system/dyn/page/tab/exportTemplate', {
    ...queryParams
  }, `tab_${new Date().getTime()}.xlsx`)
}

/**
 * 动态获取table中的列进行展示
 */
export function getTabCol(data) {
  return request({
    url: '/system/dyn/page/tab/col',
    method: 'post',
    data: data
  })
}
