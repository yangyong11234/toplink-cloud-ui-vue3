import request from '@/utils/request'
import {download} from '@/utils/request'

// 查询动态页面新增、修改、查看详情配置列表
export function listZsd(query) {
    return request({
        url: '/system/dyn/page/zsd/list',
        method: 'get',
        params: query
    })
}

// 查询动态页面新增、修改、查看详情配置详细
export function getZsd(id) {
    return request({
        url: '/system/dyn/page/zsd/' + id,
        method: 'get'
    })
}

// 新增动态页面新增、修改、查看详情配置
export function addZsd(data) {
    return request({
        url: '/system/dyn/page/zsd',
        method: 'post',
        data: data
    })
}

// 修改动态页面新增、修改、查看详情配置
export function updateZsd(data) {
    return request({
        url: '/system/dyn/page/zsd',
        method: 'put',
        data: data
    })
}

// 删除动态页面新增、修改、查看详情配置
export function delZsd(id) {
    return request({
        url: '/system/dyn/page/zsd/' + id,
        method: 'delete'
    })
}

export function exportZsd(queryParams) {
    download('system/dyn/page/zsd/export', {
        ...queryParams
    }, `zsd_${new Date().getTime()}.xlsx`)
}


export function exportZsdTemplate(queryParams){
    download('system/dyn/page/zsd/exportTemplate', {
        ...queryParams
    }, `zsd_${new Date().getTime()}.xlsx`)
}

// string转json
export function strToJson(str) {
    var json = (new Function("return " + str))();
    return json;
}