import request, {download} from '@/utils/request'

// 查询动态页面table合并列表
export function listTabItem(query) {
  return request({
    url: '/system/dyn/tabItem/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面table合并详细
export function getTabItem(id) {
  return request({
    url: '/system/dyn/tabItem/' + id,
    method: 'get'
  })
}

// 新增动态页面table合并
export function addTabItem(data) {
  return request({
    url: '/system/dyn/tabItem',
    method: 'post',
    data: data
  })
}

// 修改动态页面table合并
export function updateTabItem(data) {
  return request({
    url: '/system/dyn/tabItem',
    method: 'put',
    data: data
  })
}

// 删除动态页面table合并
export function delTabItem(id) {
  return request({
    url: '/system/dyn/tabItem/' + id,
    method: 'delete'
  })
}

export function exportTabItem(queryParams) {
  download('system/dyn/tabItem/export', {
    ...queryParams
  }, `tabItem_${new Date().getTime()}.xlsx`)
}

export function exportTabItemTemplate(queryParams) {
  download('system/dyn/tabItem/exportTemplate', {
    ...queryParams
  }, `tabItem_${new Date().getTime()}.xlsx`)
}

