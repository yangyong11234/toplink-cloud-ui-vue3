import request, {download} from '@/utils/request'

// 查询动态页面查询条件配置列表
export function listQry(query) {
  return request({
    url: '/system/dyn/page/qry/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面查询条件配置详细
export function getQry(id) {
  return request({
    url: '/system/dyn/page/qry/' + id,
    method: 'get'
  })
}

// 新增动态页面查询条件配置
export function addQry(data) {
  return request({
    url: '/system/dyn/page/qry',
    method: 'post',
    data: data
  })
}

// 修改动态页面查询条件配置
export function updateQry(data) {
  return request({
    url: '/system/dyn/page/qry',
    method: 'put',
    data: data
  })
}

// 修改动态页面查询条件排序
export function updateSort(data) {
  return request({
    url: '/system/dyn/page/qry/updateSort',
    method: 'put',
    data: data
  })
}

// 删除动态页面查询条件配置
export function delQry(id) {
  return request({
    url: '/system/dyn/page/qry/' + id,
    method: 'delete'
  })
}

export function exportQry(queryParams) {
  download('system/dyn/page/qry/export', {
    ...queryParams
  }, `qry_${new Date().getTime()}.xlsx`)
}

export function exportQryTemplate(queryParams) {
  download('system/dyn/page/qry/exportTemplate', {
    ...queryParams
  }, `qry_${new Date().getTime()}.xlsx`)
}


