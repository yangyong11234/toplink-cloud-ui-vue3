import request, {download} from '@/utils/request'

// 查询动态页面各种方法配置列表
export function listFun(query) {
  return request({
    url: '/system/dyn/page/fun/list',
    method: 'get',
    params: query
  })
}

// 查询动态页面各种方法配置详细
export function getFun(id) {
  return request({
    url: '/system/dyn/page/fun/' + id,
    method: 'get'
  })
}

// 新增动态页面各种方法配置
export function addFun(data) {
  return request({
    url: '/system/dyn/page/fun',
    method: 'post',
    data: data
  })
}

// 修改动态页面各种方法配置
export function updateFun(data) {
  return request({
    url: '/system/dyn/page/fun',
    method: 'put',
    data: data
  })
}

// 删除动态页面各种方法配置
export function delFun(id) {
  return request({
    url: '/system/dyn/page/fun/' + id,
    method: 'delete'
  })
}

export function exportFun(queryParams) {
  download('system/dyn/page/fun/export', {
    ...queryParams
  }, `fun_${new Date().getTime()}.xlsx`)
}

export function exportFunTemplate(queryParams) {
  download('system/dyn/page/fun/exportTemplate', {
    ...queryParams
  }, `fun_${new Date().getTime()}.xlsx`)
}

