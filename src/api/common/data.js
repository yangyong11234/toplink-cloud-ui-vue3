import request from '@/utils/request'
import {download,jsonDownload} from '@/utils/request'

// 查询列表
export function listInfo(query, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName + '/list',
        method: 'get',
        params: query
    })
}

// 查询详细
export function getInfo(id, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName +'/'+  id,
        method: 'get'
    })
}

// 新增
export function addInfo(data, businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName ,
        method: 'post',
        data: data
    })
}

// 修改
export function updateInfo(data, businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName ,
        method: 'put',
        data: data
    })
}

// 逻辑删除
export function delInfo(id, businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName +'/'+ id,
        method: 'delete'
    })
}

// 真实删除
export function delData(id, businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName +'/del/'+ id,
        method: 'delete'
    })
}

// 导入
export function exportInfo(formData,params,headers,businessName,moduleName) {
    return request({
        url:'/'+moduleName+'/' + businessName +  '/excelImp',
        method: 'post',
        params:params,
        data: formData,
        headers: headers,
    })
}

//导出模版
// export function exportInfoTemplate(params, businessName,moduleName) {
//     return request({
//         url:'/'+moduleName+'/' + businessName +  '/exportTemplate',
//         method: 'post',
//         data: params
//     })
// }
export function exportInfoTemplate(formData,params,businessName,moduleName){
    jsonDownload('/'+moduleName+'/' + businessName +  '/excelTemp', {
        ...formData
    },{...params}, `${businessName}_${new Date().getTime()}.xlsx`)
}
// export function exportInfoTemplate(queryParams, businessName,moduleName){
//     download('/'+moduleName+'/' + businessName +  '/exportTemplate', {
//         ...queryParams
//     }, `${businessName}_${new Date().getTime()}.xlsx`)
// }

// 导出数据
// export function exportData(formData,params,businessName,moduleName){
//     jsonDownload('/'+moduleName+'/' + businessName +  '/excelExp', {
//         ...formData
//     },{...params}, `${businessName}_${new Date().getTime()}.xlsx`)
// }

export function exportData(formData,params,businessName,moduleName) {
    return request({
        url: '/'+moduleName+'/' + businessName +  '/excelExp',
        method: 'post',
        params:params,
        data: formData
    })
}