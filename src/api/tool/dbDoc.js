// 导出参数
import request from "@/utils/request";

export function exportHtml(dataSourceId) {
  return request({
    url: '/designer/tool/db-doc/export-html/' + dataSourceId,
    method: 'get',
    responseType: 'blob'
  })
}

export function exportWord(dataSourceId) {
  return request({
    url: '/designer/tool/db-doc/export-word/' + dataSourceId,
    method: 'get',
    responseType: 'blob'
  })
}

export function exportMarkdown(dataSourceId) {
  return request({
    url: '/designer/tool/db-doc/export-markdown/' + dataSourceId,
    method: 'get',
    responseType: 'blob'
  })
}
