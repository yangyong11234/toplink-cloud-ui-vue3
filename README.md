<h4 align="center">基于 Vue3/Element Plus 和 Spring Boot/Spring Cloud & Alibaba 前后端分离的分布式微服务架构</h4>


## 平台简介

* 前端技术栈 [Vue3]。
* 后端技术栈。

## 前端运行

# 进入项目目录
cd TOPLINK-CLOUD-UI-VUE3

# 安装yarn
npm install -g yarn

# 安装依赖
npm install --registry=https://registry.npmmirror.com
||
yarn install --registry=https://registry.npmmirror.com

# 启动服务
yarn dev

# 构建测试环境 yarn build:stage
# 构建生产环境 yarn build:prod
# 前端访问地址 http://localhost:80
```

## 系统模块

~~~
com.toplink     
├── toplink-ui              // 前端框架 [80]
├── toplink-gateway         // 网关模块 [8080]
├── toplink-auth            // 认证中心 [9200]
├── toplink-api             // 接口模块
│       └── toplink-api-system                          // 系统接口
├── toplink-common          // 通用模块
│       └── toplink-common-core                         // 核心模块
│       └── toplink-common-datascope                    // 权限范围
│       └── toplink-common-datasource                   // 多数据源
│       └── toplink-common-dyn                          // mybatis框架基础方法实现
│       └── toplink-common-excel                        // excel报表
│       └── toplink-common-exception                    // 异常捕获
│       └── toplink-common-log                          // 日志记录
│       └── toplink-common-mapper                       // 监听指定文件，当指定文件mapper发生变化后动态加载
│       └── toplink-common-redis                        // 缓存服务
│       └── toplink-common-security                     // 安全模块
│       └── toplink-common-swagger                      // 系统接口
├── toplink-modules         // 业务模块
│       └── toplink-system                              // 系统模块 [9201]
│       └── toplink-gen                                 // 代码生成 [9202]
│       └── toplink-job                                 // 定时任务 [9203]
│       └── toplink-file                                // 文件服务 [9300]
├── toplink-visual          // 图形化管理模块
│       └── toplink-visual-monitor                      // 监控中心 [9100]
├──pom.xml                  // 公共依赖
~~~

## 架构图

<img src="https://oscimg.oschina.net/oscnet/up-82e9722ecb846786405a904bafcf19f73f3.png"/>

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。


## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/cd1f90be5f2684f4560c9519c0f2a232ee8.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/1cbcf0e6f257c7d3a063c0e3f2ff989e4b3.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4148b24f58660a9dc347761e4cf6162f28f.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/5e8c387724954459291aafd5eb52b456f53.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/644e78da53c2e92a95dfda4f76e6d117c4b.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ece3fd37a3d4bb75a3926e905a3c5629055.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-92ffb7f3835855cff100fa0f754a6be0d99.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ff9e3066561574aca73005c5730c6a41f15.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5e4daac0bb59612c5038448acbcef235e3a.png"/></td>
    </tr>
</table>









```
## NGINX
#### 内网:
```
server {
    listen 80;
    server_name 192.168.100.240;

    # 根目录设置
    root /www/wwwroot/103.219.29.131;

    # 默认页面
    index index.php index.html index.htm;

    # 缓存控制，针对HTML文件
    location ~* \.(html|htm)$ {
        add_header Cache-Control "private, no-store, no-cache, must-revalidate, proxy-revalidate";
        access_log on;  # 启用访问日志
        expires -1;  # 禁用缓存
    }

    # 静态资源处理
    location /static/ {
        alias /www/wwwroot/103.219.29.131/static/;  # 静态资源路径
        expires 30d;  # 设置静态资源的缓存时间为30天
    }

    # 后端接口代理
    location /prod-api/ {
        proxy_pass http://192.168.100.240:8080/;  # 确保代理路径以斜杠结束
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        # 连接超时设置
        proxy_connect_timeout 90;
        proxy_send_timeout 90;
        proxy_read_timeout 90;
        send_timeout 90;

        # 负载均衡相关设置（如果有多个后端服务）
        # proxy_pass http://backend/;
        # proxy_set_header Host $host;
        # proxy_set_header X-Real-IP $remote_addr;
        # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # proxy_set_header X-Forwarded-Proto $scheme;
    }

    # 其他静态文件处理
    location ~* \.(gif|jpg|jpeg|png|css|js|ico)$ {
        expires 30d;
        log_not_found off;  # 静态文件404不记录
    }

    # 捕获所有请求并尝试用index.html处理
    location / {
        try_files $uri $uri/ /index.html;  # 确保使用index.html作为单页应用的入口
    }
}
```

#### 外网:
```
server
{
    listen 80;
    server_name 103.219.29.131;
    index  index.html;
    root /www/wwwroot/103.219.29.131;
     # 默认页面
    index index.php index.html index.htm;

    # 缓存控制，针对HTML文件
    location ~* \.(html|htm)$ {
        add_header Cache-Control "private, no-store, no-cache, must-revalidate, proxy-revalidate";
        access_log on;  # 启用访问日志
        expires -1;  # 禁用缓存
    }

    # 静态资源处理
    location /static/ {
        alias /www/wwwroot/103.219.29.131/static/;  # 静态资源路径
        expires 30d;  # 设置静态资源的缓存时间为30天
    }

    # 后端接口代理
    location /prod-api/ {
        proxy_pass http://192.168.100.240:8080/;  # 确保代理路径以斜杠结束
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        # 连接超时设置
        proxy_connect_timeout 90;
        proxy_send_timeout 90;
        proxy_read_timeout 90;
        send_timeout 90;

        # 负载均衡相关设置（如果有多个后端服务）
        # proxy_pass http://backend/;
        # proxy_set_header Host $host;
        # proxy_set_header X-Real-IP $remote_addr;
        # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # proxy_set_header X-Forwarded-Proto $scheme;
    }

    # 其他静态文件处理
    location ~* \.(gif|jpg|jpeg|png|css|js|ico)$ {
        expires 30d;
        log_not_found off;  # 静态文件404不记录
    }

    # 捕获所有请求并尝试用index.html处理
    location / {
        try_files $uri $uri/ /index.html;  # 确保使用index.html作为单页应用的入口
    }
}
```

